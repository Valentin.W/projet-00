#Projet-0

##Gérer son projet :
- ###_Création_
    Indiquer le nom du projet.   
  
    L'URL du projet (Par utilisateurs / Groupes).    
  
    Description du projet.
  
    Option de visibilité (Public / Privé).   
  
    Ajouter (ou pas) un README (Consignes ou autres à éditer).  
  
- ###_Gestion_
    __Branche__ : Copie modifiable d'un même fichier (Souvent d'un __master__).
  
    __Master__  : Rendu final d'un fichier.
  
  - Faire des __branches__ d'un meme fichier pour faciliter le travail en équipe et voir les modifications apportés par chaqu'un.       
    
  __Merge request__ : Demande fait au chef de projet afin de modifier un __master__ par une de ces __branches__ lors d'un push.
  
  - Si la branche rendu se base sur un __master__ antérieur alors cela créera une erreur pour la corriger :
    Update le __master__ -> Retourner sur la __branche__ -> VCS -> Git -> Merge changes en prenant la __master__ -> régler les conflits. 
    (Sur Gitlab) Settings -> Visibility, project features, permissions : permet de modifier les permissions.
  
- ###_Edition_ (Avec WebStorm)   
    Cloner le fichier voulu avec https sur gitlab.
    
    Puis sur WebStorm -> Get from VCS -> Copier le lien

    Appliquer les changements : Commit (Ajouter un message indicatif du changement) puis push le(s) dossier(s) modifiés.

- ###_Markdown_
*test* = *  *  
**test** = ** **  
_test_ = _ _  
__test__ = __ __   
`const titi = 4 ` = ``
```javascript 
const titi = 4    =```language
                      Code
                   ```
```
>Test = >

- ###Raccourcis utiles 

  - CTRL - k : git-commit
  - CTRL - MAJ - K : git-push
  - CTRL - T : git-pull / update
