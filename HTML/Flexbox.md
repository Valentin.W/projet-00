# *FlexBox*

### __Une *class* d'une *div* = une *box*__
- ### display
  Permet de définir l'état d'une *box*
    - `flex` `inline` `block` (flex sera le plus utilisé)    
      Toujours l'appliquer avant de faire les commande ci-dessous.
- ## FlexBox commandes
    - ### justify-content
      Permet de deplacer les *boxs* sur l'axe principal (De base horizontal).
        -  `flex-start`     : Deplacer au début de l'axe.
        -  `flex-end`       : Déplacer à la fin de l'axe.
        -  `center`         : Déplacer au millieu de l'axe.
        -  `space-around`   : Distancer les *boxs* à égale distance entre celles-ci.
        -  `space-between`  : Distancer les *boxs* à égale distance entre celles-ci par rapport à l'axe.
        -  `space-evenly`   : Distancer les *boxs* à égale distance entre celles-ci et des limites de l'axe.
    - ### align-items
      Permet de déplacer les *boxs* sur l'axe transversal (De base vertical).
        -  `flex-start` `flex-end` `center`
        -  `baseline`       : Les *boxs* s'alignent à la ligne de base du conteneur.
        -  `stretch`        : Les *boxs* sont étirés pour s'adapter au conteneur.
    - ### flex-direction
      Permet de changer la direction d'un axe ou d'inverser les axes.
        -  `row`            : Les *boxs* sont disposés dans la même direction que le texte.
        -  `row-reverse`    : Les *boxs* sont disposés dans la direction opposée au texte.
        -  `column`         : Les *boxs* sont disposés de haut en bas.
        -  `column-reverse` : Les éléments sont disposés de bas en haut.
    - ### order
      Permet de changet l'ordre d'une *box* sur l'axe.
      Ex: .box {order : changement de position ;}
    - ### align-self
      Permet de Deplacer un type de *box* spécifique comme `align-self`.
    - ### flex-wrap
      Permet d'aligner les *boxs* sur une meme ligne ou sur plusieurs ligne si nécessaire.
      -`nowrap`           : Toutes les *boxs* sont tenus sur une seule ligne.
      -`wrap`             : Les *boxs* s'étalent sur plusieurs lignes au besoin.
      -`wrap-reverse`     : Les *boxs* s'étalent sur plusieurs lignes dans l'ordre inversé.
    - ### flex-flow
      Permet d'utiliser les catégories `flex-wrap` + `flex-direction`.
      Ex : {flex-flow : `wrap` `direction`}
    - ### align-content
  Permet de déplacer plusieurs ligne de boites en même temps.         
  - `flex-start` `flex-start` `center` `space-between` `space-around` `stretch`.