# _Bootstrap_

-   ## Installer _Bootstrap_
    - _link_ le fichier html avec le css de bootstrap (dans le head).

    - Ajouter le plugin juste avant /body.

    *Trouvable sur le site de bootstrap :
    https://getbootstrap.com/docs/5.0/getting-started/introduction/

-   ## Utilisation _Bootstrap_
    Pour appliquer une commande de _bootstrap_ il faut la mettre en tant que _class_.

    Ex: <div class="commande" ...

-   ## _Grid_ avec _Bootstrap_
    X = Taille variable.
    - Toujours commencer par le _container_ qui représente une grille entière.
    - Puis les _row_ qui sont les rangés du _container_(axe horizontal).
    - Enfin les _col_ représentant les colonnes des lignes (axe vertical).
    - ### _Grid_ Responsive
      Une _grid_ responsive est une _grid_ qui change la taille des _col_ selon le Breakpoint(la taille de l'écran).

            - xs/auto = .col-X
            - s       = .col-sm-X
            - m       = .col-md-X
            - l       = .col-lg-X
            - xl      = .col-xl-X

    - ### Fonctionnement des _col_
      Sur une ligne il peut y avoir jusqu'a 12 _col_.

      Une _col_ de base prends toute la place possible de la ligne.

      Si il y a plusieurs _col_ elles se partagent équitablement la place sur la ligne.

      Pour définir la taille de la _col_ :
      > .col-X

      *Ne pas appliquer la taille de la _col_ en meme temps qu'une _class_ de base (faites une autre _col_ à
      l'interieur, dans laquelle vous mettrez la _class_).
        - Les _col_ ont un _margin_ de base entre elles appelés : _gutter_ faisant 30px modifiable avec m/p(_margin_/_pagin_)x/y(axe)-X
          Ex : .mx-30.
        - .w-100 permet de sauter une ligne.
        - .h-100 permet de garder la _col_ sur toutes les lignes de la _row_.
        - les commandes des _flexbox_ sont applicables sur les _col_.
          Ex: justify-content-start
        