# _CSS Part 1_

- ## Fichier _CSS_
    - New -> Stylesheet -> _CSS_
    - Doit être lier à un fichier _HTML_
    - Dans le fichier _HTML_ :
      -> _link_ (dans head) -> _rel_ ="type du dossier(ici stylesheet) & _href_ ="localisation du fichier.css"

- ## Modifier le fichier _HTML_ avec le fichier _CSS_
    - Schéma de modification dans le fichier
      Ex : `p { color = red;}`
    -  _Balise_ { le type modification = modification; }

- ## Class
    - "Balise" modifiable pour répertorier les _balise_.
    - `.Class {type de modif = modif;}`
        * Les _balise_ ont la priorités sur les _class_.
      * On peut mettre autant de _class_ que souhaiter sur n'importe quel type de _balise_.
    