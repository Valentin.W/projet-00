# *HTML Part 1*

- ## Fichier *HTML*
    - _DOCTYPE_ = Type du dossier (En l'occurance html).
    - _lang_    = Languages en 2 lettres du dossier.

- ## Head
    - _charset_ = Norme ("UTF-8").
    - _Title_   = Nom de la page.

- ## Body
    - _header_  = Haut de page.
    - _div_     = Millieu de page.
    - _footer_  = Bas de page.
        * Il peut y avoir plusieurs fois une même structure.
- ## _Balise_  de Texte
    - _p_       = Integration de texte.
    - _b_       = Integration de texte en gras.
    - _h1_      = integration de texte avec une police élevée.
    - _h2_      = Integration de texte avec une police élevée < _h1_.
    - _a_       = Integration de lien (_href_ = URL du lien & Nom de forme du lien).
    - _img_     = Integration d'image (_src_  = localisation où URL de l'image & _alt_ = description de l'image pour le
      réferencement).
        * Pour integrer une image :
            - Créer un dossier dans le projet. - Copier (l'/les) image (s) dans le dossier. - Ajouter dans _src_ la
              localistion de l'image.
    - _ul_       = Liste textuel non numérotés (_li_ pour ajouter du texte à chaque colonne)
    - _ol_       = Liste textuel numérotés (_li_ pour ajouter du texte à chaque colonne)
    