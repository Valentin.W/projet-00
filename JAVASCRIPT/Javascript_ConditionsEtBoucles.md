# Conditions et Boucles.

## Conditions :

- Pour créer une condition on utilise le mot clé : _if_(condition){réaction} (La condition doit être vrai pour que la
  réaction soit activé.)


- Pour continuer un _if_ par son inverse ou une autre valeur on utilise : _else if_(condition){réaction}.


- Pour continuer un _if_ par son contraire seulement on utilise : _else_{réaction}.


- Une variable n'existe que dans le bloc d'instructions où elle est déclarée.

### _Ternaire_ :

- Le _ternaire_ est un opérateur conditionnel permettant de faire en une ligne des conditions composé seulement de _if_
  et _else_ en une seule ligne de code :  (condition) ? réactionVraie : réactionFausse; .

### _Switch_ :

- Les _switchs_ permettent de faire des conditions _else if_ de manière plus courte :  
  _switch_(variableConditionnelle){
  _case_ conditionLaVariable; réaction;
  _break_; }


- _break_ permet d'arréter le switch au moment où celui-ci est lù.


- _case_ permet d'insérer un cas dans un switch.


- On peut ajouter un comportement par défaut si aucune des conditions n'est réalisée avec le mot clé : _default_.
  (Pour utiliser plusieurs conditions pour un même résultat il suffit de les empiler au même rang.)

## Boucles :

- Les boucles permettent de répeter des actions plusieurs fois au cours du code.


- Afin de créer une boucle on utilise le mot clé : _for_([expressionInitial]); [condition]; [expressionIncrémentente])
  {instructions}  
  (Toujours mettre une condition ayant une fin.)


- Afin de crèer une boucle se répetant tant que : une condition soit remplie; on utilise : 
_do_{instructions}_while_(condition)


- On peut aussi créer une boucle _while_ :
  _while_(condition){instructions}
(_break_ marche aussi dans les boucles.)


  