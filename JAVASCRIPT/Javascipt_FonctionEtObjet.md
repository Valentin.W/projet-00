# _Fonctions_ et _Objet_

## _Fonction_

- Une _Fonction_ est un ensemble d'instuctions effectuant une tâche ou calculant une valeur.

### Créer une _Fonction_ :

```javascript
function maFonction(variablesUtilisés) {
    Instructions;
...
    return valeurRetournées
}
```

- On peut utiliser une valeur par défaut dans les variablesUtilisés. Ex:
  ```javascript
  function maFonction ( variableUtilisés = "x")
- Utilisations :    
  Ex :

```javascript
let newVariable = maFonction(variableUtilisés : valeurs); 
```

( La _fonction_ n'est pas qu'utilisable pour une assigniation de variable ! )

### _Fonction_ anonyme

- Une _fonction_ anonyme est une _fonction_ ne portant pas de nom.

### _Fonction_ _callback_

- Une _fonction_ dites callback est une _fonction_ mise en variableUtilisés. Ex :

```javascript
function myCallBack(variablesUtilisés) {
    instuctions;
...
}

function maFonction(variablesUtilisés, callback) {
    Instructions;
...
    return valeurRetournées
}

maFonction(variablesUtilisés, myCallBack)
```

Ici dans l'appel de maFonction on utilise pas la fonction myCallback on l'appelle juste.

### _arguments_

_arguments_ est un objet représentant toutes les variablesUtilisés d'une fonction (L'utilisation est la même qu'avec une
string).

## _Objet_

- Un objet est une representation des données sous forme de clé-valeurs chaque caractéristique liée à un objet est
  nommé "propriété" et chaque action liée "méthode".


- Créer un objet :

```javascript
  let objet = {
    propriété1: valeur,
    propriété2: valeur,
    propriété3: valeur,

    méthode: function () {
        instruction;
    }
};
```

- On peut accéder aux différentes méthode et propriété de l'objet :

```javascript
objet.propriété1 == objet[propriété1] ;
objet.propriété2 == objet[propriété2] ;
objet.méthode() ;
```
(On peut créer une propriété en dehors de l'objet de la meme manière que l'exemple précédent si le nom de la méthode ou
de la propriété n'est pas déjà pris.)

- _this_ permet d'indiquer l'objet courant.
```javascript
let objet = {
    propriété1: valeur,
    propriété1: valeur,
    propriété3: valeur,

    méthode: function () {
        instructions;
        this.propriété1;
    }
}; 
```

