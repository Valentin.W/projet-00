# _String_ (Javascript)

- ## Syntaxes
  _str_ = ' texte ' ;   
  _str_ = " texte " ;   
  _str_ = \` texte \` ;
   * Essayer de rester constant sur l'utilisation d'une seule syntaxe.

- Pour échapper un caratère il faut ajouter un " \ " avant le caractère ex : _str_ = 'C\'est bon'.
- Pour mettre à la ligne il suffit d'ajouter " \n " et pour mettre un espace : " \r ".

- ## Concaténation
    Permet d'additioner les _string_ ex : _str1_ + _str2_ + "blabla" = [ str1 str2 blabla ].
- ## Propriétés
    Va ici servir à modifier une _string_.   
    Exemple d'utilisation : str.propriété; .
  - .length permet de connaitre la longueur d'une chaine caractère.
  - .toUpperCase() permet de mettre ne majuscule une chaine de caractère.
  - .toLowerCase() permet de mettre en minuscule une chaine de caractère.
    
- ## Position dans une chaine de caractère.
    - Exemple : _str_[position du caractère];
    - Ne pas oublier que le premier caractère d'une _string_ est 0 donc le dernier caractère d'une string est : length - 1.
    
- ## Postion d'une string dans une string.  
    - Exemple : str.indexOf(chaine de caractère voulu); .
    - Indique la place du premier caractère de la chaine de caractère voulu.


    