# Manipulation de tableau

## Spread Operator

- Le spread operator(...) permet d'étaler toutes les valeurs d'un tableau où d'un objet.     
  Ex :

```javascript
let arr = [1, 2, 3];
let arr2 = [4, 5, 6];
let arr3 = [...arr, ...arr2];
arr3 = [1, 2, 3, 4, 5, 6];
console.log(...arr3);
-> 1 / 2 / 3 / 4 / 5 / 6 
```    

- Le spread operator peut être utilisé dans des objets comme ceci :

````javascript
let object0 = {
    truc: 1,
    truc0: 0,
};
let object = {
    ...object0
}
console.log(object.truc); -> 1
````

- Pour copier un objet où un tableau il faut utiliser le *spread operator*.

````javascript
let object1 = {}
let object2 = object(Ne pas faire.)
let object2 = {...object}
````

## Décomposition

- La décomposition permet d'assigner les valeurs d'un tableau facilement.       
  Ex :

````javascript
let a, b, reste;
let arr = [1, 2, 3, 4]
    [a, b,...reste] = arr;
console.log(a); -> 1
console.log(b); -> 2
console.log(reste); -> [3, 4]
````

- On peut déclarer les variables pendant l'étape de décomposition et on peut assigner des valeurs par défaut.

````javascript
const [a, b = 0, ...c] = [1, 2, 3, 4, 5];
````

- Dans un objet :

````javascript
let truc1, truc3;
let obj = {
    truc1: 1,
    truc2: 2,
    truc3: 3,
};
({truc1, truc3} = obj);
console.log(truc1, truc3) -> 1 / 3
````

- On peut assigner un nouveau nom aux variables contenant les propriétés.

```javascript
let obj = {
    truc1: 1,
    truc2: 2,
    truc3: 3,
};
let {truc1: Un, truc3: Trois} = obj;
console.log(Un, Trois) -> 1 / 3
```

## Méthodes de manipulation de tableau

- ### .forEach()

    - La méthode forEach permet d'éffectuer une fonction pour chaque élément d'un tableau.

    - Parcourir un tableau :    
      Ex :
  ```javascript
      let arr = [1, 2, 5, 6]
        arr.forEach(function (value, index, array){
        console.log(index, value, array) -> 
        0, 1 / 1, 2 / 2, 5 / 3, 6 / [1, 2, 5, 6]   
        });
  ```
- ### .find() / .findIndex()

    - La méthode find permet de trouver le premier élément dans un tableau en fonction d'une condition en boolean.
    
    Dans cet exemple on utilise la méthode find pour n'afficher que les mots contenant un 'o'.      
    ````javascript
    let arr = ["Jean", "Sam", "Steve", "Tom"];
   
    let res = arr.find(function (name){return name.includes('o');); 
    console.log(res);  -> "Tom" 
    ````
    
    - La méthode findIndex permet de trouver le premier élément dans un tableau en fonction d'une condition et renvoie son
      index.
    
        La méthode findIndex à la meme utilisation que la méthode find mais renverra l'index au lieu de la valeur. 
- ### .map()

    - La méthode map permet de créer et de modifier un tableau à partir d'un tableau.
    
    Dans cet exemple on crée un tableau arr2 à partir des valeurs de arr multipliés par 2 : 
    ```javascript
    let arr = [1, 2, 3, 4, 5]
    let arr2 = arr.map(function (value) { return value * 2; })
    console.log(arr2) -> [2, 4, 6, 8, 10]
    ```
- ### .reduce()

    - La méthode reduce permet de réduire un tableau sous une certaine valeur ou une autre forme.
    
    Dans cet exemple on change le tableau arr en un nombre qui serait la somme de toutes ses valeurs :
    
    ```javascript
    let arr = [1, 2, 3, 4, 5];
    accumulation = arr.reduce(function(accumulateur: number, value: number) { 
    console.log(value,accumulateur);
    accumulateur = acc + value;
    return acc;
    },0) -> 1 ,0 / 2, 1 / 3, 3 / 4, 6 / 10, 5
    console.log(accumulation); -> 15  
    ```  
    On peut aussi créer un tableau en mettant '[]' en dernier éléments de la méthode reduce (Mais généralement la méthode map sera mieux pour cet utilisation.).
- ### .filter()

    - La méthode filter permet de filtrer les éléments d'un tableau sous une condition.
    
    Dans cet exemple on crée un tableau arrFiltered qui contient tout les éléments pair du tableau arr. 
    ```javascript
    let arr = [1, 2, 3, 4, 5];
    let arrFiltered = arr.filter(function (element, index){
        return !(element % 2);
     });
    console.log(arrFiltered) -> [2, 4]
    ``` 
- ## .some() / .every()

    - La méthode some permet de retourner true si l'une des valeurs corresponds à une condition.
        
    Ici on cherche à savoir si dans le tableau arr il y a au moins un éléments pair.
        
    ```javascript
        let arr = [1, 2, 3, 4, 5];
        let boolean = arr.some(function (value) { return !(value % 2) })
        console.log(boolean) -> true
    ```
  (La méthode some teste chacune des valeurs jusqu'à trouvé une valeur correspondant à la condition.)
    - La méthode every permet de retourner true si toutes les valeurs correspondent à une condition.
    la méthode every à la même utilisation que la méthode some mais vérifie si toutes les valeurs correspondent.