# _Javascript_

- _Javasript_ est un language de script servant principalement dans les pages web intéractives.

- _Javascript_ fait son apparition en 1995 son objectif : dynamiser les sites web via intéractions et animations.

- Avec _html_ et _css_ il fait partis des technologies les plus utilisés.

- Si une entreprise utilise un site il est fort probable qu'elle cherche un développeur Javascript pour développement,
  évolution de site déja éxistant où maintenance.

- Avant appelés Livescript puis renommés en _Javascript_.

- Java /= Javascript.

- Utilisés coté client pour un site web réactif où développer une interface utilisateur réactive.

- Utilisés coté serveur : Développer exécuter et héberger tout type d'application où site.

- Framework javascript : _*Angular*_ et _*React*_ les plus utilisés ajout de partie dynamique et complexe.
  _*Nodejs*_ permet d'exécutés du code Javascript cotés serveur et _*Trijs*_ sert pour la 3D.

- Standard : _ECMA_ international dans la spécification _ECMA262_.

## 1- _Définition_

- _Standard_ : Régit le fonctionnement et la syntaxe du language.
- _Polyfill_ : Permet d'ajouter un code récent sur un navigateur qui ne le supporte nativement.
- _Transpiler_ : Type de compilateur traduisant un code _standardisés_ vers un autre type de code (_*Babel*_ et _*Webpack*_ =
  _transpiler_).
- _Instructions_ : Ligne de code se terminant par un :";".
- Bloc d'_instructions_ : Paquet d'_instructions_ délimités par : "{}"
- _Variable_ : Boite mémoire pouvant contenir une valeur et peut changer de valeur au cours du code.
- _Constant_ : Boite mémoire pouvant contenir une valeur et ne peut changer de valeur au cours du code.
- _Modulo_ : Signe % est le reste d'une division euclidienne.
## 2- _Code Javascript_

- Créer un fichier.js

- Un code _Javascript_ s'éxecute toujours dans l'ordre : du haut vers le bas.

- console.log('...'); permet d'afficher des élements sur la _console_.
  (pour mettre plusieurs éléments séparez les avec une ",")

- Pour lier un fichier.js à un fichier.html il faut ajouter une balise script indiquer la src.  
  (On peut écrire du code _javascript_ dans la balise script en définissant le type du code.)

- Déclarer une _variable_ : let nomDeLaVariable = "Valeur";
- Déclarer une _constant_ : constant nomDeLaVariable = "Valeur";
  (Les valeurs sont utilisables en utilisant le nom des conteneurs)
- Nommer les _variables_ :
  - Le nom de la _variable_ doit représenter son contenu.
  - En Anglais.
  - Pas de chiffre en premier caractère.
  - Seul le :"_" est autorisés en caractères spécial.
  - Excepté le premier à chaque nouveau mot on commence par une majuscule.
  - Ne pas nommer une variable exactement comme un mot clé de _Javascript_.

- Types de _variables_ :

  - _Number_ : les chiffres et nombres.
  - _String_ : chaines de caractères.
  - _Boolean_ : Déclencheur à activé via true ou false.
  - _Object_  : Pour contenir un objet {}.
  - _Array_  : Tableau contenant plusieurs objets []. On peut faire un "console.log(typeof nomDeLaVariable)" afin de
    connaitre le type de la variable.

- Opérateurs :
  - "+" = addition.
  - "-" = soustraction.
  - "*" = multiplication.
  - "/" = division.

- _Incrémentation_ / _Décrémentation_ :

  - ++variable = ajoutera 1 à la _variable_ pendant l'_instruction_.
  - variable++ = ajoutera 1 à la _variable_ apres l'_instruction_.
  - --variable = enlèvera 1 à la _variable_ pendant l'_instruction_.
  - variable-- = enlèvera 1 à la _variable_ apres l'_instruction_.

- Plus unaire :

  - +variable = change le type de la _variable_ de _String_ à _Number_.
  - -variable = change le type de la variable de _Number_ à _String_.

- Comparaison :

  - var1 == var2 = true ou false : égalité simple (meme entre une _String_ et un _Number_).
  - var1 != var2 = true ou false : inégalité simple (meme entre une _String_ et un _Number_).
  - var1 === var2 = true ou false : égalité strict.
  - var1 !== var2 = true ou false : inégalité strict.
  - var1 > var2 = true ou false : strictement supérieur.
  - var1 < var2 = true ou false : strictement inférieur.  
    (On peut utiliser les "<=" et ">=")
- Opérateurs Logiques :

  - ET Logique => expression1 && expression2 = true ou false si l'une est fausse où toutes alors cela
    renverra un false.

  - OU Logique => expression1 || expression2 = true ou false en fonction si l'une est vrai où toutes alors cela
    renverra un true.

  - NON Logique => !expression donne l'inverse de l'expression ("!!" sert à savoir si une variable est définie).

  (Les parenthéses sont une priorité dans l'instruction)

- Affectation :

  - Affectation simple : test = 3;
  - Affectation avec Addition : test += 3; = test=test+3;
  - Affectation avec Addition : test -= 3; = test=test-3;
  - Idem pour multiplication et division.
  - Reste par modulo : test %= 3; = test=test%3;
  