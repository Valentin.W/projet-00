# Dom

- Le DOM, qui signifie Document Object Model, est une interface de programmation qui est une représentation du HTML
  d'une page web et qui permet d'accéder aux éléments de cette page web et de les modifier avec le langage.


- Afin d'optimiser les performances d'une page Web il est préférable de mettre les intégrations javascript à la fin du
  body c'est-à-dire apres le head et l'intégration des éléments html.

- On doit toujours vérifier que le dom est bien chargé avant de manipler les éléments à l'intérieur.

- Pour vérifier que le dom se charge correctement on utilise :

```javascript
console.log(document.querySelector(élementDeLaPage));
```

- Si le script java se situe avant l'élément de la page sélectionnés on peut utiliser :

```javascript
console.log(window)
window.onload = function () {
    console.log(document.querySelector(élementDeLaPage));
}
```

Cela permettra d'afficher les éléments apres que la page ait fini de charger.

On peut aussi utiliser cette méthode :

```javascript
window.addEventListener('DOMContentLoaded', function () {
    console.log(document.querySelector(élementDeLaPage));
})
```

Dans la console on pourra voir si l'élément indiqués est chargé où non.

- Pour sélectionner un élément de la page avec son id :

```html

<div id="truc" class="chose"></div>
```

```javascript
const truc = document.getElementById(truc);
console.log(truc);
->
<div id="truc" class="chose"></div>
``` 

Avec javascript on peut prendre des class en paramètres mais on préféra utiliser les id.

(On ne peut assigner qu'un id unique pour chaque élément.)

- Pour sélectionner un élément de la page avec sa classe :

```html

<div id="truc" class="chose"></div>
<div class="chose"></div>
<div class="chose"></div>
<div class="chose"></div>
```

```javascript
const truc = document.getElementsByClassName(chose);
console.log(chose);
->
HTMLCollection(4) [div.chose, div.chose, div.chose, div.chose]
``` 

Quand il y a plusieurs éléments d'une même classe cela nous donne un HTMLCollection qui est un tableau et est donc
manipulable en utilisant ses propriétés propre.

- Afin de sélectionner un où plusieurs éléments :

```html

<div id='1' class="chose"></div>
<div id='3' class="chose quelque"></div>
<div id='2' class="chose"></div>
<div id='4' class="chose quelque"></div>
```

```javascript
const chose1 = document.querySelector('.chose');
console.log(chose1);
->
<div id="1" class="chose"></div>
const chose2 = document.querySelector('.chose.quelque');
console.log(chose2);
->
<div id='3' class="chose quelque"></div>
const chose3 = document.querySelector('#1');
console.log(chose3);
->
<div id='1' class="chose"></div>
const chose4 = document.querySelector("div");
console.log(chose4);
->
<div id='1' class="chose"></div>
const touteChose = document.querySelectorAll('.chose');
console.log(touteChose);
->
NodeList(4) [
...]
const touteChose2 = document.querySelectorAll('div.chose.quelque');
console.log(touteChose2);
->
NodeList(2) [
...]
``` 

- Afin de manipuler le style d'un élément :

```javascript
  const chose = document.querySelector(".chose");
chose.typemodification.propriété;

chose.style.backgroundColor
:
blue;
setInterval(function (paramètres) {
    instructions;
}
milliSecondesDIntervalle
)
;
```

Ici on a changé le fond du premier élément avec la classe chose en bleu.

- La fonction setInterval permet d'exécuter une fonction toutes les millisecondes assignées.

- Pour manipuler les classes des éléments :

```html

<div id='1' class="chose"></div>
<div id='3' class="chose quelque"></div>
```

```javascript
chose.classList.add("quelque");
chose.classList.remove("quelque");
```

Ici on a ajouter la classe quelque au possesseur de la classe chose puis on l'a enlevé.

- Pour créer un élément au dom :

```javascript
let chose = document.createElement(tagDeLElement);
chose.classList.add('quelque');
chose.innerText = 'Texte';
chose.innerHTML = <strong>Gras
    <strong>;
        document.body.appendChild(chose);
```

La commande appendChild permet d'ajouter un élément au document où à un autre élément déjà existant.

```html

<button id="createSquare">Ajouter un carré</button>
<div id="redSquare"></div>
```

```javascript
const redSquareContainer = document.getElementById('redSquare');
const btnEl = document.getElementById(createSquare)
btnEl.addEventListener('click', function () {
    redSquareContainer.appendChild(createSquare())
})

function createSquare() {
    const el = document.createElement('div');
    el.classList.add(square);
    return el;
}
```

Ici on crée un bouton créant des carrés dans le dans la div redSquareContainer.

- Pour avoir tout les évenements : https://developer.mozilla.org/en-US/docs/Web/Events

### function arrow

- arrow est une fonction qui va lier le this de son contexte à son propre this .

```javascript
let user = {
    name: 'Jean';
    notes: [12, 14, 15];

    getNotes: function () {
        console.log(this);
        this.notes.forEach(notes)
    =>
        {
            console.log(this);
            console.log(this.name + 'a eu' + notes)
        }
    }
}
```

Dans cet exemple on utilise une fonction getNotes pointant avec this l'objet dans lequel la fonction se trouve puis on
établit une nouvelle fonction à l'intérieur de celle-ci qui a comme argument les notes de l'objet. 

Normalement avec un this simple on ne pourrait pas accéder à la propriété name mais en utilisant une fonction arrow pointant sur le this
précédant la fonction qui lui est liés à l'objet, on peut accéder aux propriétés de celui-ci. 