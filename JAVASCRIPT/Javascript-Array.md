# _Array_ (Javascript)

- Un _Array_ est un tableau pouvant stocké toute types de _variables_.
- Un rang du tableau est un _index_ il est possible de voir la taille d'un tableau avec la commande _length_.
- Il est conseillé de préférence de stocker des éléments du meme type dans un tableau.
- Il est possible de remplir un tableau avec la commande _.fill_
- Le dernier rang d'un tableau est _length_ -1.
- Pour ajouter une valeur à un tableau on peut utiliser _.push_ qui ajoute à la dernière position une valeur.
- Afin de supprimer ou remplacer une ou plusieurs valeurs d'un tableau à partir d'un certain rang on utilise _.splice_ .     
  Ex : arr.splice[start: rangx, deleteCount: nombreDeSuppression, items: "valeurRemplacante"]; (Mettre -1 à rangx supprime le dernier élément.)
- ## Créer un tableau
    - Un tableau se crée avec des : "[]"   
      Ex : const arr = [1, 2, 3];
    - Pour créer un tableau vide il faut utiliser le mot-clé : "_new_".  
      Ex : const arr = _new_ Array(arraylength: taille);  (Si plusieurs valeurs sont données cela créera un tableau normal
      avec les valeurs indiquées.)
- ## Utiliser les _index_

    - Pour cibler une valeur d'un tableau soit viser un index on utilise sa position dans le tableau (Rappel qu'un
      tableau commence par le rang 0).  
      Ex: arr[0];
      
      