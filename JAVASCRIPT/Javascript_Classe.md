#Classe

- Une classe est un type de fonction permettant de créer des objets (C'est un modèle d'objet).

- Declarer une classe exemple :
```javascript
class Classe {
    un = 1;
    truc = "truc";
    propriété = false;
};
let classe = new Classe()

```
(Les classes se nomme généralement avec une majuscule à la première lettres.)

- On peut modifier les propriétés des objets créer à partir d'un classe sans influencer les autres objets avec la même classe (Les propriétés de la classe sont des valeurs par défaut).

## constructor()

- La commande constructor permet de configurer les objets créer à partir d'une classe.

Exemple : 
```javascript
class Classe {
    par1 = "truc";
    par2 = 0;
    par3 ;
    par4 = "coucou"
    constructor(par1, par2, par3 = false){
        this.par1 = par1;
        this.par2 = par2;
        this.par3 = par3;
    }
}
let classe = new Classe("chose", 3, true)
console.log(classe); -> classe{par1 = "chose"; par2 = 3; par3 = true; par4 = "coucou";} 
```

La commande constructor est réservé à cette utilisation donc on ne peut pas nommer une méthode "constructor".
## Méthodes

- Une méthode est une fonction associée à un objet.

Exemple :
```javascript
class Classe {
    par1 = "truc";
    par2 = 0;
    par3 ;
    par4 = "coucou"
    constructor(par1, par2, par3 = false){
        this.par1 = par1;
        this.par2 = par2;
        this.par3 = par3;
    }
    méthode1(str) {
        console.log(this.par1 + 'mots'+ str);
    }
}
let classe = new Classe("chose", 3, true)
classe.méthode1("String"); -> "chose"+"mot"+"string"
```

##Héritage

- L'héritage permet de créer des classes intégrant une autre classe.
- On utilise le mot-clé super pour appeler la classe parente.

Exemple : 
```javascript
class Classe {
    par1 = "truc";
    par2 = 0;
    par3;
    par4 = "coucou"

    constructor(par1, par2, par3 = false) {
        this.par1 = par1;
        this.par2 = par2;
        this.par3 = par3;
    }
}

class Classe2 extends Classe{
    
    constuctor(par1, par2){
        super(par1, par2, par3 = false);
    }
}
```

- La sous-classe hérite ainsi de toutes les propriétés et méthode de la principale.

##static

- Les propriétés et méthode static ne sont pas accessible au niveau des instances, elles ne sont donc accessible 
  qu'en appelant la classe où elles ont été crées.
- En général on utilise des propriétés static pour établir des constantes ou des méthodes utilitaires.

Exemple :
```javascript
class Classe {
    par1 = "truc";
    par2 = 0;
    par3;
    static par4 = "coucou"

    constructor(par1, par2, par3 = false) {
        this.par1 = par1;
        this.par2 = par2;
        this.par3 = par3;
    }
    static test(test){
        return test > 0;
    }
}

class Classe2 extends Classe{
    
    constuctor(par1, par2){
        super(par1, par2, par3 = false);
    }
}
console.log(Classe.par4, Classe.test(9)); -> "coucou", false
```

- Ici la variable par4 et la méthode test ne sont accessible seulement en appelant la classe : Classe 
et cela même si Classe2 à hérité des propriétés et des méthodes de Classe.